#ifndef EW_PREFERENCES_H
#define EW_PREFERENCES_H
#include <string>

class EwPreferences {
	public:
		static EwPreferences* Instance();

		int getUpdateInterval();
		void setUpdateInterval(int val);
		void readFromFile();
		void writeToFile();

	private:
		EwPreferences();
		EwPreferences(EwPreferences const&){};
//		EwPreferences& operator=(EwPreferences&){};
		static EwPreferences* m_pInstance;

		int updateInterval;
};
#endif
