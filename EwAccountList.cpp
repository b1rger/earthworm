#include <EwAccountList.h>
#include <EwLogging.h>
#include <iostream>
#include <fstream>

EwAccountList* EwAccountList::m_pInstance = NULL;

EwAccountList* EwAccountList::Instance() {
	if (!m_pInstance)
		m_pInstance = new EwAccountList;
	return m_pInstance;
}

Statusnet* EwAccountList::SnInstance(int accountid) {
	EWLOG("In EwAccountList::SnInstance("+inttostr(accountid)+")", 8);
	if (accountid == 0) {
		if (!this->defaultsn) {
			this->defaultsn = new Statusnet();
		}
		return this->defaultsn;
	} else {
		for (std::list<EwAccount>::iterator iter=accounts.begin(); iter!=accounts.end(); iter++) {
			EwAccount ewa = *iter;
			if (iter->id == accountid) {
				if (!iter->sn) {
					EWLOG("Creating new Statusnet Instance for Account ID "+inttostr(accountid), 8);
					iter->sn = new Statusnet(this->getAccountIdentifier(accountid), this->getAccountHttps(accountid));
				}
				return iter->sn;
			}
		}
	}
}

EwAccountList::EwAccountList() {
	EWLOG("In EwAccountList::EwAccountList()", 8);
	this->readFromFile();
}

void EwAccountList::writeToFile() {
	EWLOG("In EwAccountList::writeToFile()", 8);
	xmlpp::Document document;
	xmlpp::Element* root = document.create_root_node("Accounts");

	for (std::list<EwAccount>::iterator iter=accounts.begin(); iter!=accounts.end(); iter++) {
		EwAccount ewa = *iter;
		EWLOG("Writing account "+ewa.identifier+" to file", 9);

		xmlpp::Element* account = root->add_child("Account");;
		xmlpp::Element* identifier = account->add_child("Identifier");
		identifier->add_child_text(ewa.identifier);

		//xmlpp::Element* api = account->add_child("Api");
		//api->add_child_text(ewa.api);

		xmlpp::Element* id = account->add_child("ID");
		id->add_child_text(inttostr(ewa.id));

		xmlpp::Element* enab = account->add_child("Enabled");
		std::string enabled;
		ewa.enabled ? enabled = "True" : enabled = "False";
		enab->add_child_text(enabled);

		xmlpp::Element* https = account->add_child("Https");
		std::string httpsen;
		ewa.https ? httpsen = "True" : httpsen = "False";
		https->add_child_text(httpsen);

		xmlpp::Element* sendfrom = account->add_child("SendFrom");
		std::string sendfromen;
		ewa.sendfrom ? sendfromen = "True" : sendfromen = "False";
		sendfrom->add_child_text(sendfromen);
	}
	document.write_to_file_formatted("Accounts.xml");
}

void EwAccountList::readFromFile() {
	EWLOG("In EwAccountList::readFromFile()", 8);
	std::ifstream ifile("Accounts.xml");
	if (ifile) {
		xmlpp::DomParser parser;
		parser.parse_file("Accounts.xml");
		if (parser) {
			xmlpp::Element* root = parser.get_document()->get_root_node();
			xmlpp::Node::NodeList list = root->get_children();
			for (xmlpp::Node::NodeList::iterator iter = list.begin(); iter != list.end(); iter++) {
				const xmlpp::Element* element = dynamic_cast<const xmlpp::Element*>(*iter);
				if (element) {
					EwAccount ewa = this->parseXmlAccount(element);
					accounts.push_back(ewa);
				}
			}
		}
	}
	EwAccount ewa;
}

EwAccountList::EwAccount EwAccountList::parseXmlAccount(const xmlpp::Element* account) {
	EWLOG("In EwAccountList::parseXmlAccount(const xmlpp::Element* account)", 9);
	EwAccount ewa;

	xmlpp::Node::NodeList children = account->get_children();
	for (xmlpp::Node::NodeList::iterator iter = children.begin(); iter != children.end(); iter++) {
		xmlpp::Node* child = *iter;
		const xmlpp::Element* element = dynamic_cast<const xmlpp::Element*>(*iter);
		if (element) {
			if (element->has_child_text()) {
				std::string name = element->get_name();
				if (name=="Identifier") {
					ewa.identifier = element->get_child_text()->get_content().c_str();
				}
				if (name=="Api") {
					//ewa.api = element->get_child_text()->get_content().c_str();
				}
				if (name=="ID") {
					int id;
					std::stringstream(element->get_child_text()->get_content().c_str())>> id;
					ewa.id = id;
				}
				if (name=="Enabled") {
					(element->get_child_text()->get_content().compare("True") == 0) ? ewa.enabled = true : ewa.enabled = false;
					EWLOG("Setting Account "+ewa.identifier+" enabled to: "+inttostr(ewa.enabled), 9);
				}
				if (name=="Https") {
					(element->get_child_text()->get_content().compare("True") == 0) ? ewa.https = true : ewa.https = false;
				}
				if (name=="SendFrom") {
					(element->get_child_text()->get_content().compare("True") == 0) ? ewa.sendfrom = true : ewa.sendfrom = false;
				}
			}
		}
	}
	return ewa;
}

std::string EwAccountList::inttostr(int i) {
	std::stringstream intstream;
	intstream<<i;
	return intstream.str();
}

std::list<EwAccountList::EwAccount> EwAccountList::getAccounts() {
	EwLogging::Instance()->log("In EwAccountList::getAccounts", 8);
	return this->accounts;
}

EwAccountList::EwAccount EwAccountList::getAccount(int id) {
	EwAccount ewa;
	for (std::list<EwAccount>::iterator iter=accounts.begin(); iter != accounts.end(); iter++) {
		ewa = *iter;
		if (ewa.id == id) {
			break;
		}
	}
	return ewa;
}

EwAccountList::EwAccount EwAccountList::getAccount(std::string identifier) {
	EwAccountList::EwAccount ewa;
	for (std::list<EwAccountList::EwAccount>::iterator iter=accounts.begin(); iter != accounts.end(); iter++) {
		ewa = *iter;
		if (ewa.identifier == identifier) {
			break;
		}
	}
	return ewa;
}

void EwAccountList::setAccountEnabled(int id) {
	for (std::list<EwAccount>::iterator iter=accounts.begin(); iter != accounts.end(); iter++) {
		if ((*iter).id == id) {
			(*iter).enabled = true;
		}
	}
}

void EwAccountList::setAccountEnabled(std::string identifier) {
	for (std::list<EwAccount>::iterator iter=accounts.begin(); iter != accounts.end(); iter++) {
		if ((*iter).identifier == identifier) {
			(*iter).enabled = true;
		}
	}
}

int EwAccountList::getLastId() {
	if (accounts.size() > 1)
		return this->accounts.back().id;
	return 0;
}

bool EwAccountList::removeAccount(std::string identifier) {
	for (std::list<EwAccount>::iterator iter=accounts.begin(); iter != accounts.end(); iter++) {
		if ((*iter).identifier == identifier) {
			accounts.erase(iter);
			return true;
		}
	}
	return false;
}
bool EwAccountList::removeAccount(int id) {
	for (std::list<EwAccount>::iterator iter=accounts.begin(); iter != accounts.end(); iter++) {
		if ((*iter).id == id) {
			accounts.erase(iter);
			return true;
		}
	}
	return false;
}

int EwAccountList::addAccount(std::string identifier) {
	EwAccount ewa;
	ewa.identifier = identifier;
	ewa.id = this->getLastId()+1;
	ewa.enabled = false;
	accounts.push_back(ewa);
	return ewa.id;
}

int EwAccountList::addAccount() {
	return this->addAccount("user@server.tld");
}

std::string EwAccountList::getAccountIdentifier(int id) {
	for (std::list<EwAccount>::iterator iter=accounts.begin(); iter != accounts.end(); iter++) {
		if ((*iter).id == id) {
			return (*iter).identifier;
		}
	}
	return NULL;
}

bool EwAccountList::updateAccountIdentifier(int id, std::string identifier) {
	EWLOG("In EwAccountList::updateAccountIdentifier("+inttostr(id)+", "+identifier+")", 8);
	for (std::list<EwAccount>::iterator iter=accounts.begin(); iter != accounts.end(); iter++) {
		if ((*iter).id == id) {
			(*iter).identifier = identifier;
			return true;
		}
	}
	return false;
}

bool EwAccountList::updateAccountEnabled(int id, bool enabled) {
	EWLOG("In EwAccountList::updateAccountEnabled("+inttostr(id)+", "+inttostr(enabled)+")", 8);
	for (std::list<EwAccount>::iterator iter=accounts.begin(); iter != accounts.end(); iter++) {
		if ((*iter).id == id) {
			(*iter).enabled = enabled;
			return true;
		}
	}
	return false;
}

bool EwAccountList::updateAccountHttps(int id, bool https) {
	EWLOG("In EwAccountList::updateAccountHttps("+inttostr(id)+", "+inttostr(https)+")", 8);
	for (std::list<EwAccount>::iterator iter=accounts.begin(); iter != accounts.end(); iter++) {
		if ((*iter).id == id) {
			(*iter).https = https;
			return true;
		}
	}
	return false;
}


bool EwAccountList::getAccountEnabled(int id) {
	EWLOG("In EwAccountList::getAccountEnabled("+inttostr(id)+")", 8);
	for (std::list<EwAccount>::iterator iter=accounts.begin(); iter != accounts.end(); iter++) {
		if ((*iter).id == id) {
			return (*iter).enabled;
		}
	}
	return false;
}

bool EwAccountList::getAccountHttps(int id) {
	EWLOG("In EwAccountList::getAccountHttps("+inttostr(id)+")", 8);
	for (std::list<EwAccount>::iterator iter=accounts.begin(); iter != accounts.end(); iter++) {
		if ((*iter).id == id) {
			return (*iter).https;
		}
	}
	return false;
}


int EwAccountList::getIdToIdentifier(std::string identifier) {
	EWLOG("In EwAccountList::getIdToIdentifier("+identifier+")", 8);
	for (std::list<EwAccount>::iterator iter=accounts.begin(); iter!= accounts.end(); iter++) {
		if ((*iter).identifier == identifier) {
			return (*iter).id;
		}
	}
	return 0;
}

int EwAccountList::getIdOfFirstAccount() {
	EWLOG("In EwAccountList::getIdOfFirstAccount()", 8);
	return this->accounts.front().id;
}

void EwAccountList::setAccountPassword(int id, std::string password) {
	EWLOG("In EwAccountList::setAccountPassword("+inttostr(id)+", "+password+")", 8);
	for (std::list<EwAccount>::iterator iter=accounts.begin(); iter!= accounts.end(); iter++) {
		if ((*iter).id== id) {
			iter->password = password;
			iter->sn->setPassword(password);
		}
	}
}

std::string EwAccountList::getAccountPassword(int id) {
	EWLOG("In EwAccountList::getAccountPassword("+inttostr(id)+")", 8);
	for (std::list<EwAccount>::iterator iter=accounts.begin(); iter!= accounts.end(); iter++) {
		if ((*iter).id== id) {
			return iter->password;
		}
	}
}

void EwAccountList::setAccountSendFrom(int id) {
	EWLOG("In EwAccountList::setAccountSendFrom("+inttostr(id)+")", 8);
	for (std::list<EwAccount>::iterator iter=accounts.begin(); iter!= accounts.end(); iter++) {
		if ((*iter).id== id) {
			iter->sendfrom = !(iter->sendfrom);
		}
	}
	writeToFile();
}
