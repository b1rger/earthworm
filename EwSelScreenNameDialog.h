#ifndef EW_SEL_SCREEN_NAME_DIALOG_H
#define EW_SEL_SCREEN_NAME_DIALOG_H
#include <gtkmm.h>

class EwSelScreenNameDialog {
	public:
		EwSelScreenNameDialog();
		bool screenNameSet();
		std::string getScreenName();

	private:
		Gtk::Dialog *dialog;
		Gtk::Button *okbutton, *cancelbutton;
		Gtk::Entry *screennameentry;
		bool screennameset;
		std::string screen_name;
		void okButtonClicked();
		void cancelButtonClicked();
};
#endif
