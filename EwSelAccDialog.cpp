#include <EwSelAccDialog.h>
#include <EwLogging.h>
#include <EwAccountList.h>

EwSelAccDialog::EwSelAccDialog() {
	Glib::RefPtr<Gtk::Builder> builder = Gtk::Builder::create_from_file("data/EwSelAccDialog.glade");
	Gtk::Box *rbtnbox;
	Gtk::RadioButton::Group rbgroup;
	Gtk::Button *okbtn, *cncbtn;
	builder->get_widget("dialog", dialog);
	builder->get_widget("rbtnbox", rbtnbox);
	builder->get_widget("okbtn", okbtn);
	builder->get_widget("cncbtn", cncbtn);
	id = -1;

	std::list<EwAccountList::EwAccount> accounts = EwAccountList::Instance()->getAccounts();
	for (std::list<EwAccountList::EwAccount>::iterator iter = accounts.begin(); iter != accounts.end(); iter++) {
		if (iter->enabled) {
			if (id==-1) 
				id = iter->id;
			std::cout<<"adding radiobtn"<<std::endl;
			Gtk::RadioButton *account = new Gtk::RadioButton(rbgroup, (*iter).identifier);
			account->signal_clicked().connect(sigc::bind(sigc::mem_fun(*this, &EwSelAccDialog::onrbtnclicked), (*iter).id));
			rbtnbox->pack_start(*account);
		}
	}

	okbtn->signal_clicked().connect(sigc::mem_fun(*this, &EwSelAccDialog::on_okbtn_clicked));
	cncbtn->signal_clicked().connect(sigc::mem_fun(*this, &EwSelAccDialog::on_cncbtn_clicked));
	dialog->show_all_children();

	dialog->run();
}

void EwSelAccDialog::on_okbtn_clicked() {
	dialog->hide();
}

void EwSelAccDialog::on_cncbtn_clicked() {
	this->id = -1;
	dialog->hide();
}

void EwSelAccDialog::onrbtnclicked(int id) {
	EWLOG("EwSelAccDialog::onrbtnclicked: "+inttostr(id), 8);
	this->id = id;
}

int EwSelAccDialog::getId() {
	EWLOG("EwSelAccDialog::getId()", 8);
	return this->id;
}

std::string EwSelAccDialog::inttostr(int i) {
	std::stringstream intstream;
	intstream<<i;
	return intstream.str();
}
