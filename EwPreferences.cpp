#include <EwPreferences.h>

EwPreferences* EwPreferences::m_pInstance = NULL;

EwPreferences* EwPreferences::Instance() {
	if (!m_pInstance)
		m_pInstance = new EwPreferences;
	return m_pInstance;
}

EwPreferences::EwPreferences() {
	this->updateInterval = 300;
}

int EwPreferences::getUpdateInterval() {
	return this->updateInterval;
}

void EwPreferences::setUpdateInterval(int val) {
	this->updateInterval = val;
}
