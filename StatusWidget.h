#include "libsn/statusnet.h"
#include <webkit/webkit.h>
#include <glib.h>
#include <gtkmm.h>

class StatusWidget : public Gtk::Frame {
	public:
		StatusWidget(Statusnet::Status status);
	private:
		std::string getStringFromFile(std::string filename);
		std::string templateReplace(std::string haystack, std::string needle, std::string newstring);
		std::string parse(Statusnet::Status status);
		static gboolean navigation_policy_decision_requested_cb(WebKitWebView* web_view, WebKitWebFrame* web_frame, WebKitNetworkRequest* request, WebKitWebNavigationAction* action, WebKitWebPolicyDecision* decision);
};
