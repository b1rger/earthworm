#ifndef _LIBSN_STATUSNET_H
#define _LIBSN_STATUSNET_H
#include <iostream>
#include <list>
#include <libxml++/libxml++.h>


int writer(char *data, size_t size, size_t nmemb, std::string *buffer);
class Statusnet { 
	class Api { 
		public: 
			std::string name; 
			std::string apiLink; 
			bool preferred; 
			std::string blogID; 
	};
	public: 
		struct TimelineArguments {
			TimelineArguments() : trim_user(false), include_entities(false), count(20), since_id(0), max_id(0), page(0), include_rts(false), exclude_replies(false), contributor_details(false), user_id(0), screen_name("") {}
			bool trim_user;
			bool include_entities;
			int count;
			int since_id;
			int max_id;
			int page;
			bool include_rts;
			bool exclude_replies;
			bool contributor_details;
			int user_id;
			std::string screen_name;
		};
		struct User {
			int id;
			std::string name;
			std::string screen_name;
			std::string location;
			std::string description;
			std::string profile_img_url;
			std::string url;
			bool protect;
			int followers_count;
			int friends_count;
			time_t created_at;
			int favourites_count;
			int utc_offset;
			std::string time_zone;
			int statuses_count;
			bool following;
			bool blocking;
			bool notifications;
			std::string profile_url;
			std::string profile_background_color;
			std::string profile_text_color;
			std::string profile_link_color;
			std::string profile_sidebar_fill_color;
			std::string profile_background_tile;
			std::string profile_background_image_url;
		};
		struct Status {
			std::string text;
			bool truncated;
			std::string created_at;
			int in_reply_to_status_id;
			std::string source;
			int id;
			int in_reply_to_user_id;
			std::string in_reply_to_screen_name;
			long lon;
			long lat;
			bool favorited;
			User user;
			std::string statusnet_html;
			int conversation_id;
		};
		typedef std::list<Status> StatusList;

		Statusnet(std::string identifier, bool https = false, std::string password = "");
		Statusnet();
		~Statusnet();
		std::string findRoot(std::string servername = "");
		std::string findApi();
		std::list<Api> listApis(std::string *servername = NULL);
		void setPassword(std::string password);


		// timeline resources
		StatusList getPublicTimeline(bool trim_user = false, bool include_entities = false);
		StatusList getHomeTimeline(int count = 20, int since_id = 0, int max_id = 0, int page = 0, 
				bool trim_user = false, bool include_rts = false, bool include_entities = false, 
				bool exclude_replies = false, bool contributor_details = false);
		StatusList getMentions(int count = 20, int since_id = 0, int max_id = 0, int page = 0,
				bool trim_user = false, bool include_rts =false, bool include_entities = false,
				bool contributor_details = false);
		StatusList getUserTimeline(int user_id = 0, std::string screen_name = "", int since_id = 0,
				int count = 0, int max_id = 0, int page = 0, bool trim_user = false, bool include_rts = true,
				bool include_entities = false, bool exclude_replies = false, bool contributor_details = false);
		StatusList getRetweetsOfMe(int count = 0, int since_id = 0, int max_id = 0, int page = 0,
				bool trim_user = false, bool include_entities = false);

		// status resources
		Statusnet::Status showStatus(int id, bool trim_user = false, bool include_entities = false);
		Statusnet::Status updateStatus(std::string status, int in_reply_to_status_id = 0, long lat = 0, long lon = 0, 
				std::string place_id = "", bool display_coordinates = false, bool trim_user = false, 
				bool include_entities = false);
		Statusnet::Status destroyStatus(int id, bool include_entities = false, bool trim_user = false);
		Statusnet::Status retweetStatus(int, bool include_entities, bool trim_user);

		// parsing
		User parseUser(xmlpp::Node* user);
		Status parseStatus(xmlpp::Node* status);

	private:
		std::string identifier;
		std::string password;
		bool https;
		std::string mainUrl;
		std::string root;
		std::list<Api> apiList;

		// timeline
		StatusList getTimeline(std::string url, std::list<std::string> *postdata = NULL, std::list<std::string> *getdata = NULL);
		// status
		Statusnet::Status getStatus(std::string url, std::list<std::string> *postdata = NULL);

		std::string getDefaultApiUrl(std::string *servername = NULL);
		std::string getMainUrl();
		std::string getUsername();
		std::string curlHttpget(std::string url, std::list<std::string> *postdata = NULL, std::list<std::string> *getdata = NULL);
		//xmlpp::Node traverseXmlName(xmlpp::Node *pNode, std::list<std::string> identifiers);
		//
		
		// helper
		std::string inttostr(int i);
		std::string longtostr(long l);
		int strtoint(std::string s);
		bool strtobool(std::string s);
		bool curlTestforAuth(std::string url);
};

#endif
