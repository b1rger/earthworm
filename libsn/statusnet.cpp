#include "statusnet.h"
#include "exceptions.h"
#include <iostream>
#include <sstream>
#include <curl/curl.h>
#include <pcrecpp.h>
#include <string>
#include <list>
#include <libxml++/libxml++.h>
#include <sstream>

Statusnet::Statusnet(std::string identifier, bool https, std::string password) {
	this->identifier = identifier;
	this->https = https;
	this->password = password;
	this->root = this->findRoot();
}

Statusnet::Statusnet() {
}

Statusnet::~Statusnet() {
}

std::string Statusnet::findRoot(std::string servername) {
	std::istringstream iss;
	if (servername != "") {
		iss.str(this->curlHttpget(servername));
	} else {
		iss.str(this->curlHttpget(this->getMainUrl()));
	}

	std::string root;
	std::string line;
	pcrecpp::RE re("<link rel=\"EditURI\" type=\"application\/rsd.?xml\" href=\"(.+)\"/>");
	while (getline(iss, line)) {
		if (re.PartialMatch(line, &root)) {
			break;
		}
	}
	return root;
}

Statusnet::User Statusnet::parseUser(xmlpp::Node* user) {
	Statusnet::User userobj;
	const xmlpp::Element* nodeElement = dynamic_cast<const xmlpp::Element*>(user);

	if (nodeElement) {
	xmlpp::Node::NodeList attributes = user->get_children();
	for (xmlpp::Node::NodeList::iterator iter = attributes.begin(); iter != attributes.end(); iter++) {
		const xmlpp::Node* node = *iter;
		const xmlpp::Element* nodeElement = dynamic_cast<const xmlpp::Element*>(node);
		if (nodeElement) {
			std::string name = nodeElement->get_name();
			if (nodeElement->has_child_text()) {
			if (name == "id") {
				std::stringstream(nodeElement->get_child_text()->get_content().c_str()) >> userobj.id;
			} else if (name == "name") {
				userobj.name = nodeElement->get_child_text()->get_content().c_str();
			} else if (name == "screen_name") {
				userobj.screen_name = nodeElement->get_child_text()->get_content().c_str();
			} else if (name == "location") {
				userobj.location = nodeElement->get_child_text()->get_content().c_str();
			} else if (name == "description") {
				userobj.description = nodeElement->get_child_text()->get_content().c_str();
			} else if (name == "profile_image_url") {
				userobj.profile_img_url = nodeElement->get_child_text()->get_content().c_str();
			} else if (name == "url") {
				userobj.url = nodeElement->get_child_text()->get_content().c_str();
			} else if (name == "protected") {
				nodeElement->get_child_text()->get_content().c_str() == "false" ? userobj.protect = false : userobj.protect = true;
			} else if (name == "followers_count") {
				std::stringstream(nodeElement->get_child_text()->get_content().c_str()) >> userobj.followers_count;
			} else if (name == "friends_count") {
				std::stringstream(nodeElement->get_child_text()->get_content().c_str()) >> userobj.friends_count;
			} else if (name == "created_at") {
				//TODO: strtotime()
			} else if (name == "favourites_count") {
				userobj.favourites_count = strtoint(nodeElement->get_child_text()->get_content().c_str());
			} else if (name == "utc_offset") {
				userobj.utc_offset = strtoint(nodeElement->get_child_text()->get_content().c_str());
			} else if (name == "time_zone") {
				userobj.time_zone = nodeElement->get_child_text()->get_content().c_str();
			} else if (name == "statuses_count") {
				userobj.statuses_count = strtoint(nodeElement->get_child_text()->get_content().c_str());
			} else if (name == "following") {
				userobj.following = strtobool(nodeElement->get_child_text()->get_content().c_str());
			} else if (name == "blocking") {
				userobj.blocking = strtobool(nodeElement->get_child_text()->get_content().c_str());
			} else if (name == "notifications") {
				userobj.notifications = strtobool(nodeElement->get_child_text()->get_content().c_str());
			} else if (name == "profile_url") {
				userobj.profile_url = nodeElement->get_child_text()->get_content().c_str();
			} else if (name == "profile_background_color") {
				userobj.profile_background_color = nodeElement->get_child_text()->get_content().c_str();
			} else if (name == "profile_text_color") {
				userobj.profile_text_color = nodeElement->get_child_text()->get_content().c_str();
			} else if (name == "profile_link_color") {
				userobj.profile_link_color = nodeElement->get_child_text()->get_content().c_str();
			} else if (name == "profile_sidebar_fill_color") {
				userobj.profile_sidebar_fill_color = nodeElement->get_child_text()->get_content().c_str();
			} else if (name == "profile_background_tile") {
				userobj.profile_background_tile = nodeElement->get_child_text()->get_content().c_str();
			} else if (name == "profile_background_image_url") {
				userobj.profile_background_image_url = nodeElement->get_child_text()->get_content().c_str();
			} else {
				std::cout<<"Statusnet: Unknown User Element: \""<<name<<"\" with content: \""<<nodeElement->get_child_text()->get_content().c_str()<<"\""<<std::endl;
			}
		} 
	}
	}
	return userobj;
	}
}

Statusnet::Status Statusnet::parseStatus(xmlpp::Node* status) {
	Statusnet::Status statusobj;
	const xmlpp::Element* nodeElement = dynamic_cast<const xmlpp::Element*>(status);
	
	if (nodeElement) {
	xmlpp::Node::NodeList attributes = status->get_children();
	for (xmlpp::Node::NodeList::iterator iter = attributes.begin(); iter != attributes.end(); iter++) {
		xmlpp::Node* node = *iter;
		const xmlpp::Element* nodeElement = dynamic_cast<const xmlpp::Element*>(*iter);
		if (nodeElement) {
			if (nodeElement->has_child_text()) {
			std::string name = nodeElement->get_name();
			if (name == "text") {
				statusobj.text = nodeElement->get_child_text()->get_content().c_str();
			} else if (name == "truncated") {
				nodeElement->get_child_text()->get_content().c_str() == "false" ? statusobj.truncated = false : statusobj.truncated = true;
			} else if (name == "created_at") {
				statusobj.created_at = nodeElement->get_child_text()->get_content().c_str();
			} else if (name == "in_reply_to_status_id") {
				std::stringstream(nodeElement->get_child_text()->get_content().c_str()) >> statusobj.in_reply_to_status_id;
			} else if (name == "source") {
				statusobj.source = nodeElement->get_child_text()->get_content().c_str();
			} else if (name == "id") {
				std::stringstream(nodeElement->get_child_text()->get_content().c_str()) >> statusobj.id;
			} else if (name == "in_reply_to_user_id") {
				std::stringstream(nodeElement->get_child_text()->get_content().c_str()) >> statusobj.in_reply_to_user_id;
			} else if (name == "in_reply_to_screen_name") {
				statusobj.in_reply_to_screen_name = nodeElement->get_child_text()->get_content().c_str();
			} else if (name == "geo") {
				//TODO:
			} else if (name == "favorited") {
				nodeElement->get_child_text()->get_content().c_str() == "false" ? statusobj.favorited = false : statusobj.favorited = true;
			} else if (name == "user") {
				statusobj.user = this->parseUser(node);
			} else if (name == "html") {
				statusobj.statusnet_html = nodeElement->get_child_text()->get_content().c_str();
			} else if (name == "conversation_id") {
				statusobj.conversation_id = strtoint(nodeElement->get_child_text()->get_content().c_str());
			}
		}
		}
	}
	} else {
		std::cout<<"status is empty"<<std::endl;
	}
	return statusobj;
}

std::list<Statusnet::Api> Statusnet::listApis(std::string *servername) {
	std::list<Api> apilist;
	std::string myroot;
	if (servername) {
		myroot = this->curlHttpget(this->findRoot("https://"+*servername));
	} else { 
		myroot = this->curlHttpget(this->root);
	}
	pcrecpp::RE("xmlns=\"http.+\"").Replace("", &myroot);
	xmlpp::DomParser parser;
	parser.parse_memory(myroot);
	if (parser) {
		xmlpp::Node* pNode = parser.get_document()->get_root_node();
		xmlpp::NodeSet nodes = pNode->find("/rsd/service/apis/api");
		for (xmlpp::NodeSet::iterator iter = nodes.begin(); iter != nodes.end(); iter++) {
			Api api;
			const xmlpp::Node* node = *iter;
			const xmlpp::Element* nodeElement = dynamic_cast<const xmlpp::Element*>(node);
			if (nodeElement) {
				const xmlpp::Element::AttributeList& attributes = nodeElement->get_attributes();
				for(xmlpp::Element::AttributeList::const_iterator iter = attributes.begin(); iter != attributes.end(); ++iter) {
					const xmlpp::Attribute* attribute = *iter;
					std::string name = attribute->get_name();
					std::string value = attribute->get_value();
					if (name == "name") {
						api.name = value;
					} else if (name == "apiLink") {
						api.apiLink = value;
					} else if (name == "preferred") {
						value == "true" ? api.preferred = true : api.preferred = false;
					} else if (name == "blogID") {
						api.blogID = value;
					}
				}
			}
			apilist.push_back(api);
		}
	}
	return apilist;
}

std::string Statusnet::getDefaultApiUrl(std::string *servername) {
	if (servername) {
		std::list<Api> apilist = listApis(servername);
		for (std::list<Api>::iterator iter = apilist.begin(); iter != apilist.end(); iter++) {
			if (iter->preferred) return iter->apiLink;
		}
		return apilist.front().apiLink;
	} else {
		if (this->apiList.empty()) {
			this->apiList = this->listApis();
		}
		std::list<Api>::iterator it;
		for (it = this->apiList.begin(); it != this->apiList.end(); it++) {
			if (it->preferred) return it->apiLink;
		}
		return this->apiList.front().apiLink;
	}
}

bool Statusnet::curlTestforAuth(std::string url) {
	CURL *curl;
	std::string *buffer = new std::string("");

	curl = curl_easy_init();
	if (curl) {
		curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, writer);
		curl_easy_setopt(curl, CURLOPT_WRITEDATA, buffer);

		curl_easy_perform(curl);
		long *httpresponse = new long();
		curl_easy_getinfo(curl, CURLINFO_HTTP_CODE, httpresponse);
		if (*httpresponse == 401) {
			return true;
		}
	}
	return false;
}

std::string Statusnet::curlHttpget(std::string url, std::list<std::string> *postdata, std::list<std::string> *getdata) {
	std::string buffer, postdatastring = "", getdatastring = "";
	CURL *curl;
	CURLcode result;

	if (postdata) {
		for (std::list<std::string>::iterator iter=postdata->begin(); iter!=postdata->end(); iter++) {
			postdatastring+=(*iter)+"&";
		}
	}
	if (getdata) {
		getdatastring = "?";
		for (std::list<std::string>::iterator iter=getdata->begin(); iter!=getdata->end(); iter++) {
			getdatastring+=(*iter)+"&";
		}
		url += getdatastring;
	}
	if (this->curlTestforAuth(url) && this->password.empty()) {
		throw sn::snnoauthex;
	}

	curl = curl_easy_init();
	if (curl) {
		if (!this->password.empty() && this->curlTestforAuth(url)) {
			std::string userpwd = this->getUsername() + ":" + this->password;
			curl_easy_setopt(curl, CURLOPT_USERPWD, userpwd.c_str());
		}
		if (postdatastring!="") {
			curl_easy_setopt(curl, CURLOPT_POST, 1);
			curl_easy_setopt(curl, CURLOPT_COPYPOSTFIELDS, postdatastring.c_str());
		}
		curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
		curl_easy_setopt(curl, CURLOPT_HEADER, 0);
		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, writer);
		curl_easy_setopt(curl, CURLOPT_WRITEDATA, &buffer);

		result = curl_easy_perform(curl);
		curl_easy_cleanup(curl);
		if (result == CURLE_OK) {
			return buffer;
		} else {
			throw sn::sncurlnoconex;
		}
	} else {
		throw sn::snnocurlinex;
	}
}

std::string Statusnet::getMainUrl() {
	std::string hostname = this->identifier.substr(this->identifier.find("@")+1);
	if (https) {
		return "https://"+hostname;
	} else {
		return "http://"+hostname;
	}
}

std::string Statusnet::getUsername() {
	return this->identifier.substr(0, this->identifier.find("@"));
}

/* timeline resources 
TODO: look for arguments
*/

/**
 * Returns the 20 most recent statuses, including retweets if they exist, from non-protected users.
 * The public timeline is cached for 60 seconds.
 * Requesting more frequently than that will not return any more data, and will count against your rate limit usage.
 * @param trim_user When set to either true, t or 1, each tweet returned in a timeline will include 
 * a user object including only the status authors numerical ID.
 * @param include_entities When set to either true, t or 1, each tweet will include a node called "entities,". 
 * This node offers a variety of metadata about the tweet in a discreet structure, including: user_mentions, urls, 
 * and hashtags.
 */
Statusnet::StatusList Statusnet::getPublicTimeline(bool trim_user, bool include_entities) {
	std::list<std::string> *getdata = new std::list<std::string>();
	if (trim_user) getdata->push_front("trim_user=true");
	if (include_entities) getdata->push_front("include_entities=true");

	std::string publictimeline = this->getDefaultApiUrl() + "/statuses/public_timeline.xml";
	return this->getTimeline(publictimeline, NULL, getdata);
}

/**
 * Returns the most recent statuses, including retweets if they exist, posted by the authenticating user 
 * and the user's they follow.
 * @param count Specifies the number of records to retrieve. Must be less than or equal to 200. Defaults to 20.
 * @param since_id Returns results with an ID greater than (that is, more recent than) the specified ID.
 * @param max_id Returns results with an ID less than (that is, older than) or equal to the specified ID.
 * @param page Specifies the page of results to retrieve.
 * @param trim_user When set to either true, t or 1, each tweet returned in a timeline will include a user 
 * object including only the status authors numerical ID. 
 * @param include_rts When set to either true, t or 1,the timeline will contain native retweets (if they exist) 
 * in addition to the standard stream of tweets.
 * @param include_entities When set to either true, t or 1, each tweet will include a node called "entities,". 
 * This node offers a variety of metadata about the tweet in a discreet structure, including: user_mentions, 
 * urls, and hashtags. 
 * @param exclude_replies This parameter will prevent replies from appearing in the returned timeline.
 * @param contributor_details This parameter enhances the contributors element of the status response to include 
 * the screen_name of the contributor. 
 */

Statusnet::StatusList Statusnet::getHomeTimeline(int count, int since_id, int max_id, 
		int page, bool trim_user, bool include_rts, bool include_entities, 
		bool exclude_replies, bool contributor_details) {
	std::cout<<"Statusnet: Fetching home timeline."<<std::endl;

	std::list<std::string> *getdata = new std::list<std::string>();
	getdata->push_front("count="+count);
	if (since_id>0) getdata->push_front("since_id="+inttostr(since_id));
	if (max_id>0) getdata->push_front("max_id="+inttostr(max_id));
	if (page>0) getdata->push_front("page="+inttostr(page));
	if (trim_user) getdata->push_front("trim_user=true");
	if (include_rts) getdata->push_front("include_rts=true");
	if (include_entities) getdata->push_front("include_entities=true");
	if (exclude_replies) getdata->push_front("exclude_replies=true");
	if (contributor_details) getdata->push_front("contributor_details=true");

	std::string hometimeline = this->getDefaultApiUrl() + "/statuses/home_timeline.xml";
	return this->getTimeline(hometimeline, NULL, getdata);
}

/**
 * Returns the 20 most recent mentions (status containing @username) for the authenticating user.
 * @param count Specifies the number of records to retrieve. Must be less than or equal to 200.
 * @param since_id Returns results with an ID greater than (that is, more recent than) the specified ID. 
 * @param max_id Returns results with an ID less than (that is, older than) or equal to the specified ID.
 * @param page Specifies the page of results to retrieve.
 * @param trim_user When set to either true, t or 1, each tweet returned in a timeline will include a user object 
 * including only the status authors numerical ID.
 * @param include_rts When set to either true, t or 1,the timeline will contain native retweets (if they 
 * exist) in addition to the standard stream of tweets.
 * @param include_entities When set to either true, t or 1, each tweet will include a node called "entities,".
 * This node offers a variety of metadata about the tweet in a discreet structure, including: user_mentions, 
 * urls, and hashtags.
 * @param contributor_details This parameter enhances the contributors element of the status response to 
 * include the screen_name of the contributor.
 */

Statusnet::StatusList Statusnet::getMentions (int count, int since_id, int max_id, int page,
				bool trim_user, bool include_rts, bool include_entities,
				bool contributor_details) {

	std::list<std::string> *getdata = new std::list<std::string>();
	getdata->push_front("count="+count);
	if (since_id>0) getdata->push_front("since_id="+inttostr(since_id));
	if (max_id>0) getdata->push_front("max_id="+inttostr(max_id));
	if (page>0) getdata->push_front("page="+inttostr(page));
	if (trim_user) getdata->push_front("trim_user=true");
	if (include_rts) getdata->push_front("include_rts=true");
	if (include_entities) getdata->push_front("include_entities=true");
	if (contributor_details) getdata->push_front("contributor_details=true");

	std::string mentions = this->getDefaultApiUrl() + "/statuses/mentions.xml";
	return this->getTimeline(mentions, NULL, getdata);
}

/**
 * Returns the 20 most recent statuses posted by the authenticating user. It is also possible to request 
 * another user's timeline by using the screen_name or user_id parameter. The other users timeline will only 
 * be visible if they are not protected, or if the authenticating user's follow request was accepted by the 
 * protected user.
 * Always specify either an user_id or screen_name when requesting a user timeline.
 * @param user_id The ID of the user for whom to return results for. Helpful for disambiguating when a valid 
 * user ID is also a valid screen name.
 * @param screen_name The screen name of the user for whom to return results for.
 * @param since_id Returns results with an ID greater than (that is, more recent than) the specified ID. 
 * @param count Specifies the number of tweets to try and retrieve, up to a maximum of 200.
 * @param max_id Returns results with an ID less than (that is, older than) or equal to the specified ID.
 * @param page Specifies the page of results to retrieve.
 * @param trim_user When set to either true, t or 1, each tweet returned in a timeline will include a user 
 * object including only the status authors numerical ID.
 * @param include_rts When set to either true, t or 1,the timeline will contain native retweets (if they exist) 
 * in addition to the standard stream of tweets
 * @param include_entities When set to either true, t or 1, each tweet will include a node called "entities,".
 * This node offers a variety of metadata about the tweet in a discreet structure, including: user_mentions, 
 * urls, and hashtags.
 * @param exclude_replies This parameter will prevent replies from appearing in the returned timeline.
 * @param contributor_details This parameter enhances the contributors element of the status response to 
 * include the screen_name of the contributor.
 */
Statusnet::StatusList Statusnet::getUserTimeline(int user_id, std::string screen_name, int since_id,
				int count, int max_id, int page, bool trim_user, bool include_rts,
				bool include_entities, bool exclude_replies, bool contributor_details) {
	std::string server;
	std::string usertimeline = "";
	if (screen_name!="") {
		server = screen_name.substr(screen_name.find("@")+1);
		screen_name = screen_name.substr(0, screen_name.find("@"));
	}

	//TODO: either user_id or screen_name have to be set
	std::list<std::string> *getdata = new std::list<std::string>();
	if (user_id>0) getdata->push_front("user_id="+inttostr(user_id));
	if (screen_name!="") getdata->push_front("screen_name="+screen_name);
	if (since_id>0) getdata->push_front("since_id="+inttostr(since_id));
	if (count>0) getdata->push_front("count="+count);
	if (max_id>0) getdata->push_front("max_id="+inttostr(max_id));
	if (page>0) getdata->push_front("page="+inttostr(page));
	if (trim_user) getdata->push_front("trim_user=true");
	if (include_rts) getdata->push_front("include_rts=true");
	if (include_entities) getdata->push_front("include_entities=true");
	if (exclude_replies) getdata->push_front("exclude_replies=true");
	if (contributor_details) getdata->push_front("contributor_details=true");

	if (server!="") {
		usertimeline = this->getDefaultApiUrl(&server) + "/statuses/user_timeline.xml";
	} else {
		usertimeline = this->getDefaultApiUrl() + "/statuses/user_timeline.xml";
	}
	return this->getTimeline(usertimeline, NULL, getdata);
}

/**
 * Returns the 20 most recent tweets of the authenticated user that have been retweeted by others.
 * @param count Specifies the number of records to retrieve. Must be less than or equal to 100.
 * @param since_id Returns results with an ID greater than (that is, more recent than) the specified ID.
 * @param max_id Returns results with an ID less than (that is, older than) or equal to the specified ID.
 * @param page Specifies the page of results to retrieve.
 * @param trim_user When set to either true, t or 1, each tweet returned in a timeline will include a user 
 * object including only the status authors numerical ID.
 * @param include_entities When set to either true, t or 1, each tweet will include a node called "entities,". 
 * This node offers a variety of metadata about the tweet in a discreet structure, including: user_mentions, 
 * urls, and hashtags.
 */
Statusnet::StatusList Statusnet::getRetweetsOfMe(int count, int since_id, 
		int max_id, int page, bool trim_user, bool include_entities) {

	std::list<std::string> *getdata = new std::list<std::string>();
	if (count>0) getdata->push_front("count="+count);
	if (since_id>0) getdata->push_front("since_id="+inttostr(since_id));
	if (max_id>0) getdata->push_front("max_id="+inttostr(max_id));
	if (page>0) getdata->push_front("page="+inttostr(page));
	if (trim_user) getdata->push_front("trim_user=true");
	if (include_entities) getdata->push_front("include_entities=true");

	std::string retweetsofme = this->getDefaultApiUrl() + "/statuses/retweets_of_me.xml";
	return this->getTimeline(retweetsofme, NULL, getdata);
}

Statusnet::StatusList Statusnet::getTimeline(std::string url, std::list<std::string> *postdata, std::list<std::string> *getdata) {
	std::string xmltimeline = this->curlHttpget(url, postdata, getdata);
	Statusnet::StatusList timeline;
	xmlpp::DomParser parser;
	parser.parse_memory(xmltimeline);
	if (parser) {
		xmlpp::Node* pNode = parser.get_document()->get_root_node();
		xmlpp::Node::NodeList list = pNode->get_children();
		for(xmlpp::Node::NodeList::iterator iter = list.begin(); iter != list.end(); ++iter) {
			const xmlpp::Element* element = dynamic_cast<const xmlpp::Element*>(*iter);
			if (element) {
				Statusnet::Status status = this->parseStatus(*iter);
				timeline.push_front(status);
			}
		}
	}
	return timeline; 
}

/* status resources
*/

/**
 * Returns a single status, specified by the id parameter below. The status's author will be returned inline.
 * @param id The numerical ID of the desired status.
 * @param trim_user When set to either true, t or 1, each tweet returned in a timeline will include a user 
 * object including only the status authors numerical ID.
 * @param include_entities When set to either true, t or 1, each tweet will include a node called "entities,". 
 * This node offers a variety of metadata about the tweet in a discreet structure, including: user_mentions, 
 * urls, and hashtags.
 * @return
 */
Statusnet::Status Statusnet::showStatus(int id, bool trim_user, bool include_entities) {
	std::list<std::string> *postdata = new std::list<std::string>();
	if (trim_user) postdata->push_front("trim_user=true");
	if (include_entities) postdata->push_front("include_entities=true");

	std::string show = this->getDefaultApiUrl() + "statuses/show/"+inttostr(id)+".xml";
	return this->getStatus(show, postdata);
}

/**
 * Updates the authenticating user's status, also known as tweeting.
 * @param status The text of your status update, typically up to 140 characters. URL encode as necessary.
 * @param in_reply_to_status_id The ID of an existing status that the update is in reply to.
 * @param lat The latitude of the location this tweet refers to. (between -90.0 and +90.0)
 * @param long The longitude of the location this tweet refers to. (beteen -180.0 and +180.0)
 * @param place_id A place in the world.
 * @param display_coordinates Whether or not to put a pin on the exact coordinates a tweet has been sent from.
 * @param trim_user When set to either true, t or 1, each tweet returned in a timeline will include a user 
 * object including only the status authors numerical ID.
 * @param include_entities When set to either true, t or 1, each tweet will include a node called "entities,". 
 * This node offers a variety of metadata about the tweet in a discreet structure, including: user_mentions, 
 * urls, and hashtags.
 * @return
 */

Statusnet::Status Statusnet::updateStatus(std::string status, int in_reply_to_status_id, long lat, long lon, 
				std::string place_id, bool display_coordinates, bool trim_user, 
				bool include_entities) {
	std::cout<<"In Statusnet::updateStatus("<<status<<")"<<std::endl;
	std::list<std::string> *postdata = new std::list<std::string>();
	postdata->push_front("status="+status);
	if (in_reply_to_status_id > 0) postdata->push_front("in_reply_to_status_id="+inttostr(in_reply_to_status_id));
	//TODO: check if they are in the expected range
	if (lat != 100) postdata->push_front("lat="+longtostr(lat));
	if (lon != 200) postdata->push_front("long="+longtostr(lon));
	if (place_id != "") postdata->push_front("place_id="+place_id);
	if (display_coordinates) postdata->push_front("display_coordinates=true");
	if (trim_user) postdata->push_front("trim_user=true");
	if (include_entities) postdata->push_front("include_entities=true");

	std::string update = this->getDefaultApiUrl() + "statuses/update.xml";
	return this->getStatus(update, postdata);
}

/**
 * Destroys the status specified by the required ID parameter. The authenticating user must be 
 * the author of the specified status. Returns the destroyed status if successful.
 * @param id The numerical ID of the desired status.
 * @param include_entities When set to either true, t or 1, each tweet will include a node called 
 * "entities,". This node offers a variety of metadata about the tweet in a discreet structure, 
 * including: user_mentions, urls, and hashtags.
 * @param trim_user When set to either true, t or 1, each tweet returned in a timeline will include 
 * a user object including only the status authors numerical ID.
 * @return
 */
Statusnet::Status Statusnet::destroyStatus(int id, bool include_entities, bool trim_user) {
	std::list<std::string> *postdata = new std::list<std::string>();
	postdata->push_front("id="+inttostr(id));
	if (include_entities) postdata->push_front("include_entities=true");
	if (trim_user) postdata->push_front("trim_user=true");
	std::string destroy = this->getDefaultApiUrl() + "statuses/destroy.xml";

	return this->getStatus(destroy, postdata);
}

/* Retweets a tweet. Returns the original tweet with retweet details embedded.
 * @param id The numerical ID of the desired status.
 * @param include_entities When set to either true, t or 1, each tweet will include a node called 
 * "entities,". This node offers a variety of metadata about the tweet in a discreet structure, 
 * including: user_mentions, urls, and hashtags.
 * @param trim_user When set to either true, t or 1, each tweet returned in a timeline will include 
 * a user object including only the status authors numerical ID.
 */

Statusnet::Status Statusnet::retweetStatus(int id, bool include_entities, bool trim_user) {
	std::list<std::string> *postdata = new std::list<std::string>();
	postdata->push_front("id="+inttostr(id));
	if (include_entities) postdata->push_front("include_entities=true");
	if (trim_user) postdata->push_front("trim_user=true");
	std::string retweet = this->getDefaultApiUrl() + "statuses/retweet.xml";

	return this->getStatus(retweet, postdata);
}

Statusnet::Status Statusnet::getStatus(std::string url, std::list<std::string> *postdata) {
	std::string xmlstatus = this->curlHttpget(url, postdata);
	std::cout<<xmlstatus<<std::endl;
	xmlpp::DomParser parser;
	parser.parse_memory(xmlstatus);
	if (parser) {
		xmlpp::Node* pNode = parser.get_document()->get_root_node();
		return this->parseStatus(pNode);
	}
}

std::string Statusnet::inttostr(int i) {
	std::stringstream intstream;
	intstream<<i;
	return intstream.str();
}

std::string Statusnet::longtostr(long l) {
	std::stringstream longstream;
	longstream<<l;
	return longstream.str();
}

int Statusnet::strtoint(std::string s) {
	int i;
	std::stringstream(s) >> i;
	return i;
}

bool Statusnet::strtobool(std::string s) {
	bool b;
	(s == "false") ? b = false : b = true;
	return b;
}

void Statusnet::setPassword(std::string password) {
	this->password = password;
}

int writer(char *data, size_t size, size_t nmemb, std::string *buffer){
	int result = 0;
	if(buffer != NULL) {
		buffer->append(data, size * nmemb);
		result = size * nmemb;
	}
	return result;
} 
