#ifndef _LIBSN_EXCEPTIONS_H
#define _LIBSN_EXCEPTIONS_H
#include <exception>

namespace sn {

class noauth_error: public std::exception {
	virtual const char* what() const throw() {
		return "This URL requires authentication (Username and Password).";
	}
} snnoauthex;

class curlnocon_error: public std::exception {
	virtual const char* what() const throw() {
		return "There are problems connecting to the server.";
	}
} sncurlnoconex;

class curlnoin_error: public std::exception {
	virtual const char* what() const throw() {
		return "Could not initialize curl.";
	}
} snnocurlinex;

}
#endif
