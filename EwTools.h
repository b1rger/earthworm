#ifndef EW_TOOLS_H
#define EW_TOOLS_H

namespace EwTools {
	enum TimelineType { Tpublic, Thome, Tmentions, Tuser, Tretweetofme };
}
#endif
