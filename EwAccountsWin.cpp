#include <EwAccountsWin.h>
#include <EwLogging.h>
#include <EwAccountList.h>
#include <gtkmm.h>
#include <libxml++/libxml++.h>
#include <iostream>
#include <fstream>

//TODO: klasse soll window extenden (schoener so)
EwAccountsWin::EwAccountsWin() {
	EWLOG("In EwAccountsWin::EwAccountsWin()", 8);
	Glib::RefPtr<Gtk::Builder> builder = Gtk::Builder::create_from_file("data/EwAccountsWin.glade");
	Gtk::Button *add, *del, *close;
	Gtk::ScrolledWindow *scrolledwindow;
	builder->get_widget("EwAccounts", window);
	builder->get_widget("add", add);
	builder->get_widget("delete", del);
	builder->get_widget("close", close);
	builder->get_widget("scrolledwindow", scrolledwindow);
	scrolledwindow->add(treeView);
	add->signal_clicked().connect(sigc::mem_fun(this, &EwAccountsWin::onAddBtnClicked));
	del->signal_clicked().connect(sigc::mem_fun(this, &EwAccountsWin::onDelBtnClicked));
	close->signal_clicked().connect(sigc::mem_fun(this, &EwAccountsWin::onCloseBtnClicked));

	liststore = Gtk::ListStore::create(m_Columns);
	treeView.set_model(liststore);
	treeView.append_column("Enabled", m_Columns.m_col_enab);
	treeView.append_column_editable("Identifier", m_Columns.m_col_identifier);
	treeView.append_column("Use Https", m_Columns.m_col_https);
	Gtk::CellRendererText *crtu;
	Gtk::CellRendererToggle *crtenab, *crthttps;
	crtenab = dynamic_cast<Gtk::CellRendererToggle*>(treeView.get_column_cell_renderer(0));
	crtenab->set_activatable();
	crtenab->signal_toggled().connect(sigc::mem_fun(this, &EwAccountsWin::onColumnEnabledToggled));
	crtu = dynamic_cast<Gtk::CellRendererText*>(treeView.get_column_cell_renderer(1));
	crtu->signal_edited().connect(sigc::mem_fun(this, &EwAccountsWin::onColumnIdentifierChanged));
	crthttps = dynamic_cast<Gtk::CellRendererToggle*>(treeView.get_column_cell_renderer(2));
	crthttps->set_activatable();
	crthttps->signal_toggled().connect(sigc::mem_fun(this, &EwAccountsWin::onColumnHttpsToggled));

	std::list<EwAccountList::EwAccount> accounts = EwAccountList::Instance()->getAccounts();
	for (std::list<EwAccountList::EwAccount>::iterator iter = accounts.begin(); iter != accounts.end(); iter++) {
		Gtk::TreeModel::Row row = *(liststore->append());
		row[m_Columns.m_col_id] = (*iter).id;
		row[m_Columns.m_col_identifier] = (*iter).identifier;
		row[m_Columns.m_col_enab] = (*iter).enabled;
		row[m_Columns.m_col_https] = (*iter).https;
	}
	
	window->show_all_children();
	Gtk::Main::run(*window);
}

void EwAccountsWin::onColumnIdentifierChanged(std::string path, std::string newtext) {
	EWLOG("In EwAccountsWin::onColumnIdentifierChanged("+path+", "+newtext+")", 8);
	Gtk::TreeModel::iterator iter = liststore->get_iter(path);
	if (iter) {
		Gtk::TreeModel::Row row = *iter;
		row[m_Columns.m_col_identifier] = newtext;
		EwAccountList::Instance()->updateAccountIdentifier(row[m_Columns.m_col_id], newtext);
	}
}
void EwAccountsWin::onColumnEnabledToggled(std::string path) {
	EWLOG("In EwAccountsWin::onColumnEnabledToggled("+path+")", 8);
	Gtk::TreeModel::iterator iter = liststore->get_iter(path);
	if (iter) {
		Gtk::TreeModel::Row row = *iter;
		row[m_Columns.m_col_enab] = !row[m_Columns.m_col_enab];
		EwAccountList::Instance()->updateAccountEnabled(row[m_Columns.m_col_id], row[m_Columns.m_col_enab]);
	}
}
void EwAccountsWin::onColumnHttpsToggled(std::string path) {
	EWLOG("In EwAccountsWin::onColumnEnabledToggled("+path+")", 8);
	Gtk::TreeModel::iterator iter = liststore->get_iter(path);
	if (iter) {
		Gtk::TreeModel::Row row = *iter;
		row[m_Columns.m_col_https] = !row[m_Columns.m_col_https];
		EwAccountList::Instance()->updateAccountHttps(row[m_Columns.m_col_id], row[m_Columns.m_col_https]);
	}
}
void EwAccountsWin::onAddBtnClicked() {
	int id = EwAccountList::Instance()->addAccount();
	Gtk::TreeModel::Row row = *(liststore->append());
	row[m_Columns.m_col_id] = id;
	row[m_Columns.m_col_identifier] = EwAccountList::Instance()->getAccountIdentifier(id);
	row[m_Columns.m_col_enab] = EwAccountList::Instance()->getAccountEnabled(id);;
}

void EwAccountsWin::onCloseBtnClicked() {
	EwAccountList::Instance()->writeToFile();
	this->window->hide();
}

void EwAccountsWin::onDelBtnClicked() {
	Gtk::TreeModel::iterator iter = treeView.get_selection()->get_selected();
	if (iter) {
		Gtk::TreeModel::Row row = *iter;
		EwAccountList::Instance()->removeAccount(row[m_Columns.m_col_id]);
		liststore->erase(iter);
	}
}
