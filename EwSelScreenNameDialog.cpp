#include "EwSelScreenNameDialog.h"

EwSelScreenNameDialog::EwSelScreenNameDialog() {
	this->screennameset = false;
	Glib::RefPtr<Gtk::Builder> builder = Gtk::Builder::create_from_file("data/EwSelScreenNameDialog.glade");
	builder->get_widget("dialog", dialog);
	builder->get_widget("screennameentry", screennameentry);
	builder->get_widget("okbutton", okbutton);
	builder->get_widget("cancelbutton", cancelbutton);

	okbutton->signal_clicked().connect(sigc::mem_fun(this, &EwSelScreenNameDialog::okButtonClicked));
	cancelbutton->signal_clicked().connect(sigc::mem_fun(this, &EwSelScreenNameDialog::cancelButtonClicked));

	int result = dialog->run();
}

void EwSelScreenNameDialog::okButtonClicked() {
	this->screen_name = screennameentry->get_text();
	this->screennameset = true;
	dialog->response(Gtk::RESPONSE_OK);
	dialog->hide();
}

void EwSelScreenNameDialog::cancelButtonClicked() {
	dialog->response(Gtk::RESPONSE_CANCEL);
	dialog->hide();
}

bool EwSelScreenNameDialog::screenNameSet() {
	return this->screennameset;
}

std::string EwSelScreenNameDialog::getScreenName() {
	return this->screen_name;
}
