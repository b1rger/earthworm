RM=/bin/rm
CPP=/usr/bin/g++
CPPOPT=-Wall -g
PKGC=$(shell which pkg-config)
LDFLAGSCURL=$(shell $(PKGC) --libs libcurl)
CFLAGSCURL=$(shell $(PKGC) --cflags libcurl)
LDFLAGSPCRE=$(shell $(PKGC) --libs libpcrecpp)
CFLAGSPCRE=$(shell $(PKGC) --cflags libpcrecpp)
LDFLAGSXML=$(shell $(PKGC) --libs libxml++-2.6)
CFLAGSXML=$(shell $(PKGC) --cflags libxml++-2.6)
LDFLAGSGTK=$(shell $(PKGC) --libs gtk+-3.0 gmodule-2.0)
CFLAGSGTK=$(shell $(PKGC) --cflags gtk+-3.0 gmodule-2.0)
LDFLAGSGTKMM=$(shell $(PKGC) --libs gtkmm-3.0)
CFLAGSGTKMM=$(shell $(PKGC) --cflags gtkmm-3.0)
LDFLAGSWEBKIT=$(shell $(PKGC) --libs webkitgtk-3.0)
CFLAGSWEBKIT=$(shell $(PKGC) --cflags webkitgtk-3.0)
LDFLAGSGTHREAD=$(shell $(PGKC) --libs gthread-2.0)
CFLAGSTHREAD=$(shell $(PKGC) --cflags gthread-2.0)
LDFLAGS=$(LDFLAGSCURL) $(LDFLAGSPCRE) $(LDFLAGSXML) $(LDFLAGSGTKMM) $(LDFLAGSWEBKIT) $(LDFLAGSTHREAD)
CFLAGS=$(CFLAGSCURL) $(CFLAGSPCRE) $(CFLAGSXML) $(CFLAGSGTKMM) $(CFLAGSWEBKIT) $(CFLAGSTHREAD) -I. 


all: earthworm

earthworm: statusnet Earthworm.cpp StatusWidget.cpp StatusWidget.h EwMainWin.cpp EwMainWin.h EwAccountsWin.cpp EwAccountsWin.h EwSelAcc.cpp EwSelAcc.h EwTlScrolledWin.cpp EwTlScrolledWin.h EwPreferences.h EwPreferences.cpp EwLogging.h EwLogging.cpp EwAccountList.cpp EwAccountList.h EwTlWorker.cpp EwTlWorker.h EwAuthDialog.cpp EwAuthDialog.h EwSelScreenNameDialog.cpp EwSelScreenNameDialog.h EwSelAccDialog.cpp EwSelAccDialog.h
	$(CPP) $(CPPOPT) $(CFLAGS) -c Earthworm.cpp StatusWidget.cpp StatusWidget.h EwMainWin.cpp EwMainWin.h EwAccountsWin.cpp EwAccountsWin.h EwSelAcc.cpp EwSelAcc.h EwTlScrolledWin.cpp EwTlScrolledWin.h EwPreferences.h EwPreferences.cpp EwLogging.h EwLogging.cpp EwAccountList.cpp EwAccountList.h EwTlWorker.cpp EwTlWorker.h EwAuthDialog.cpp EwAuthDialog.h EwSelScreenNameDialog.cpp EwSelScreenNameDialog.h EwSelAccDialog.cpp EwSelAccDialog.h
	$(CPP) $(CPPOPT) -o $@ Earthworm.o StatusWidget.o EwMainWin.o EwAccountsWin.o EwSelAcc.o EwTlScrolledWin.o EwLogging.o EwPreferences.o EwAccountList.o statusnet.o EwTlWorker.o EwAuthDialog.o EwSelScreenNameDialog.o EwSelAccDialog.o $(LDFLAGS)

statusnet: libsn/statusnet.h libsn/statusnet.cpp libsn/exceptions.h
	$(CPP) $(CPPOPT) $(CFLAGS) -c libsn/statusnet.h libsn/statusnet.cpp libsn/exceptions.h

clean:
	$(RM) *.o *.gch earthworm libsn/*.gch
