#include "libsn/statusnet.h"
#include "EwMainWin.h"
#include "EwAccountsWin.h"
#include "EwSelAcc.h"
#include "EwSelAccDialog.h"
#include "EwTools.h"
#include "EwTlScrolledWin.h"
#include "EwSelScreenNameDialog.h"
#include "StatusWidget.h"
#include <iostream>
#include <exception>
#include <gtkmm.h>
#include <webkit/webkit.h>

EwMainWin::EwMainWin() {
	Glib::RefPtr<Gtk::Builder> builder = Gtk::Builder::create_from_file("data/EwMainWin.glade");
	Gtk::MenuItem *managacc;
	Gtk::Button *ptimeline, *htimeline, *mentions, *utimeline, *retwofme;
	builder->get_widget("window1", window);
	builder->get_widget("notebook1", notebook);
	builder->get_widget("managacc", managacc);
	builder->get_widget("ptimeline", ptimeline);
	builder->get_widget("htimeline", htimeline);
	builder->get_widget("mentions", mentions);
	builder->get_widget("utimeline", utimeline);
	builder->get_widget("retwofme", retwofme);
	builder->get_widget("sendfrommenu", sendfrommenu);
	builder->get_widget("statusentry", statusentry);
	
	managacc->signal_activate().connect(sigc::mem_fun(this, &EwMainWin::onManageAccountsClicked));
	ptimeline->signal_clicked().connect(sigc::bind(sigc::mem_fun(this, &EwMainWin::createTimeLinePage), EwTools::Tpublic));
	htimeline->signal_clicked().connect(sigc::bind(sigc::mem_fun(this, &EwMainWin::createTimeLinePage), EwTools::Thome));
	mentions->signal_clicked().connect(sigc::bind(sigc::mem_fun(this, &EwMainWin::createTimeLinePage), EwTools::Tmentions));
	utimeline->signal_clicked().connect(sigc::bind(sigc::mem_fun(this, &EwMainWin::createTimeLinePage), EwTools::Tuser));
	retwofme->signal_clicked().connect(sigc::bind(sigc::mem_fun(this, &EwMainWin::createTimeLinePage), EwTools::Tretweetofme));
	statusentry->signal_activate().connect(sigc::mem_fun(this, &EwMainWin::sendStatus));

	std::list<EwAccountList::EwAccount> accounts = EwAccountList::Instance()->getAccounts();
	int i = 0;
	for (std::list<EwAccountList::EwAccount>::iterator iter = accounts.begin(); iter != accounts.end(); iter++) {
		Gtk::CheckMenuItem *cmi = new Gtk::CheckMenuItem((*iter).identifier);
		if ((*iter).sendfrom) {
			cmi->set_active();
		}
		cmi->signal_toggled().connect(sigc::bind(sigc::mem_fun(this, &EwMainWin::sendFromToggled), (*iter).id));
		sendfrommenu->attach(*cmi, 0, 1, i, i+1);
		i++;
	}

	window->show_all_children();
	Gtk::Main::run(*window);
}

void EwMainWin::onManageAccountsClicked() {
	EwAccountsWin ewaw;
}

void EwMainWin::createTimeLinePage(EwTools::TimelineType type) {
	int accountid = 0;
	if (type!=EwTools::Tuser) {
		if (EwAccountList::Instance()->getAccounts().size() > 1) {
			EwSelAccDialog ewsad;
			accountid = ewsad.getId();
		} else {
			accountid = EwAccountList::Instance()->getIdOfFirstAccount();
		}
	}
	EwTlScrolledWin *etsw = new EwTlScrolledWin(accountid, type);
	if (type == EwTools::Tuser) {
		EwSelScreenNameDialog ewsnd;
		if (ewsnd.screenNameSet())
			etsw->setScreenName(ewsnd.getScreenName());
	}
	etsw->start();
	Gtk::Grid *tabgrid = new Gtk::Grid();
	Gtk::Button *tabclose = new Gtk::Button("x");
	Gtk::Label *tablabel = new Gtk::Label(etsw->getName());
	tabgrid->attach(*tablabel, 0, 0, 1, 1);
	tabgrid->attach(*tabclose, 1, 0, 1, 1);
	tabgrid->show_all();
	this->notebook->prepend_page(*etsw, *tabgrid);
	this->notebook->show_all();
	this->notebook->prev_page();
	tabclose->signal_clicked().connect(sigc::bind(sigc::mem_fun(this, &EwMainWin::tabClose), this->notebook->get_current_page()));
}

void EwMainWin::tabClose(int pagenum) {
	EwTlScrolledWin *etsw = dynamic_cast<EwTlScrolledWin*>(this->notebook->get_nth_page(pagenum));
	etsw->setThreadInactive();
	this->notebook->remove_page(pagenum);
}

void EwMainWin::sendFromToggled(int id) {
	EwAccountList::Instance()->setAccountSendFrom(id);
}

void EwMainWin::sendStatus() {
	std::list<EwAccountList::EwAccount> accounts = EwAccountList::Instance()->getAccounts();
	for (std::list<EwAccountList::EwAccount>::iterator iter = accounts.begin(); iter != accounts.end(); iter++) {
		if ((*iter).sendfrom) {
			EwAccountList::Instance()->SnInstance(iter->id)->updateStatus(statusentry->get_text());
		}
	}
}
