#include "EwPreferences.h"
#include "EwLogging.h"
#include "EwAccountList.h"
#include "EwTlScrolledWin.h"
#include "StatusWidget.h"
#include "EwAuthDialog.h"
#include <assert.h>
#include <webkit/webkit.h>

EwTlScrolledWin::EwTlScrolledWin(int accountid, EwTools::TimelineType type) {
	EWLOG("In EwTlScrolledWin::EwTlScrolledWin(int, EwTools::TimelineType)", 8);
	this->type = type;
	this->accountid = accountid;
	this->threadactive = true;
	this->updateinterval = EwPreferences::Instance()->getUpdateInterval();
	this->vbox = new Gtk::VBox();
	init();
	add(*vbox);
}

void EwTlScrolledWin::start() {
	this->dispatcher.connect(sigc::mem_fun(*this, &EwTlScrolledWin::updateWidgets));
	this->auth_fail.connect(sigc::mem_fun(*this, &EwTlScrolledWin::authDialog));
	this->threaddone.connect(sigc::mem_fun(*this, &EwTlScrolledWin::cleanUp));

	this->m_thread = Glib::Thread::create(sigc::mem_fun(*this, &EwTlScrolledWin::worker), true);
}

void EwTlScrolledWin::init() {
	switch (this->type) {
		case EwTools::Tpublic:
			this->name = EwAccountList::Instance()->getAccountIdentifier(accountid) + " Public Timeline";
			break;
		case EwTools::Thome:
			this->name = EwAccountList::Instance()->getAccountIdentifier(accountid) + " Home Timeline";
			break;
		case EwTools::Tmentions:
			this->name = EwAccountList::Instance()->getAccountIdentifier(accountid) + " Mentions";
			break;
		case EwTools::Tuser:
			//this->name = EwAccountList::Instance()->getAccountIdentifier(accountid) + " User Timeline";
			this->name = this->timelinearguments.screen_name + "'s Timeline";
			break;
		case EwTools::Tretweetofme:
			this->name = EwAccountList::Instance()->getAccountIdentifier(accountid) + " Retweets";
			break;
		default:
			this->name = "Unknown Page";
			break;
	}
}

void EwTlScrolledWin::updateWidgets() {
	EWLOG("In EwTlScrolledWin::updateWidgets()", 8);
	Glib::Mutex::Lock lock(logMutex);
	assert(pipe.empty() == false);
	Statusnet::StatusList statuslist = pipe.front();
	pipe.pop();
	for (Statusnet::StatusList::iterator iter = statuslist.begin(); iter!=statuslist.end(); iter++) {
		Statusnet::Status s = *iter;
		if (!this->isInStatuses(s)) {
			//EWLOG("New Status.", 9);
			StatusWidget *sw = manage(new StatusWidget(s));
			this->vbox->pack_end(*sw, false, false);
			this->statuses.push_back(s);
		}
	}
	this->vbox->show_all();
}

bool EwTlScrolledWin::isInStatuses(Statusnet::Status status) {
	for (Statusnet::StatusList::iterator iter=this->statuses.begin(); iter!=this->statuses.end(); iter++) {
		Statusnet::Status s = *iter;
		if (s.id == status.id) {
			return true;
		}
	}
	return false;
}

std::list<Statusnet::Status> EwTlScrolledWin::getTimeline() {
	EWLOG("In EwTlScrolledWin::getTimeline()"+this->getName(), 7);
	switch (this->type) {
		case EwTools::Tpublic:
			return EwAccountList::Instance()->SnInstance(accountid)->getPublicTimeline();
		case EwTools::Thome:
			return EwAccountList::Instance()->SnInstance(accountid)->getHomeTimeline();
		case EwTools::Tmentions:
			return EwAccountList::Instance()->SnInstance(accountid)->getMentions();
		case EwTools::Tuser:
			return EwAccountList::Instance()->SnInstance(accountid)->getUserTimeline(this->timelinearguments.user_id, this->timelinearguments.screen_name);
		case EwTools::Tretweetofme:
			return EwAccountList::Instance()->SnInstance(accountid)->getRetweetsOfMe();
		default:
			break;
	}
}

void EwTlScrolledWin::worker() {
	EWLOG("In EwTlScrolledWin::worker()", 8);
	while (this->threadactive) {
		try {
			Glib::Mutex::Lock lock(logMutex);
			pipe.push(this->getTimeline());
			lock.release();
		} catch (std::exception& snnoauthex) {
			EWLOG("Auth needed. Leaving thread.", 8);
			auth_fail.emit();
			break;
		}
		dispatcher.emit();
		std::cout<<"dispatcher.emit()"<<std::endl;
		sleep(this->updateinterval);
	}
	threaddone.emit();
	EWLOG("Closing down thread "+this->name, 8);
}

std::string EwTlScrolledWin::getName() {
	return this->name;
}

void EwTlScrolledWin::setName(std::string name) {
	this->name = name;
}

void EwTlScrolledWin::authDialog() {
	EwAuthDialog ewad(accountid);
	if (ewad.pwSet()) {
		m_thread = Glib::Thread::create(sigc::mem_fun(*this, &EwTlScrolledWin::worker), true);
	}
}

void EwTlScrolledWin::setThreadInactive() {
	this->threadactive = false;
}

void EwTlScrolledWin::cleanUp() {
	if (!this->threadactive) {
		delete this;
	}
}

void EwTlScrolledWin::setScreenName(std::string screen_name) {
	this->timelinearguments.screen_name = screen_name;
}

void EwTlScrolledWin::setUserId(int user_id) {
	this->timelinearguments.user_id = user_id;
}
