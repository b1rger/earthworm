#include "libsn/statusnet.h"
#include "EwTlWorker.h"
#include "EwTools.h"
#include <gtkmm.h>
#include <queue>

class EwTlScrolledWin : public Gtk::ScrolledWindow {
	public:
		EwTlScrolledWin(int accountid, EwTools::TimelineType type);
		std::string getName();
		void setName(std::string name);
		void setThreadInactive();
		void start();
		void setScreenName(std::string screen_name);
		void setUserId(int user_id);
	private:
		Statusnet::TimelineArguments timelinearguments;
		EwTools::TimelineType type;
		std::string name;
		int accountid;
		int updateinterval;

		Gtk::VBox *vbox;

		Statusnet::StatusList statuses;
		Glib::Dispatcher dispatcher, auth_fail, threaddone;
		Glib::Mutex logMutex;
		std::queue<Statusnet::StatusList> pipe;
		bool m_thread;
		bool threadactive;

		bool isInStatuses(Statusnet::Status status);
		Statusnet::StatusList getTimeline();
		void init();
		void worker();
		void updateWidgets();
		void authDialog();
		void cleanUp();
};
