#include <gtkmm.h>

class EwSelAcc {
	public:
		EwSelAcc(Gtk::Window *parent);
		int getId();
	private:
		Gtk::Window *ewselaccwin;
		void on_okbtn_clicked();
		void on_cncbtn_clicked();
		void onrbtnclicked(int id);
		int id;

		std::string inttostr(int i);
};
