#ifndef _EW_TL_WORKER_H
#define _EW_TL_WORKER_H
#include <iostream>
#include <glibmm/thread.h>
#include <glibmm/dispatcher.h>
#include "EwTools.h"
#include "libsn/statusnet.h"

class EwTlWorker {
	public:
		EwTlWorker();

		void start(int accountid, EwTools::TimelineType type, Statusnet::StatusList& timeline);

		~EwTlWorker();

		Glib::Dispatcher sig_done, sig_loop;

	protected:
		void run(int accountid, EwTools::TimelineType type, Statusnet::StatusList& timeline);

		Glib::Thread * thread;
		Glib::Mutex mutex;
		bool stop;

		//die ersten zwei vl auch pointer?
		int accountid;
		EwTools::TimelineType type;
		//std::list<Statusnet::Status> *timeline;
};
#endif
