#include <EwLogging.h>
#include <iostream>

EwLogging* EwLogging::m_pInstance = NULL;

EwLogging* EwLogging::Instance() {
	if (!m_pInstance)
		m_pInstance = new EwLogging;
	return m_pInstance;
}

EwLogging::EwLogging() {
	this->lvl = 10;
}

void EwLogging::setLvl(int lvl) {
	this->lvl = lvl;
}

void EwLogging::log(std::string message, int lvl) {
	if (lvl<=this->lvl) {
		std::cerr<<message<<std::endl;
	}
}
