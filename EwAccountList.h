#ifndef _EW_ACCOUNT_LIST_H
#define _EW_ACCOUNT_LIST_H
#include "libsn/statusnet.h"
#include <string>
#include <libxml++/libxml++.h>

class EwAccountList {
	public:
/*		struct EwAccount {
		};*/
class EwAccount {
	public:
		EwAccount() { sn = NULL; };
		std::string identifier;
		std::string password;
		int id;
		bool enabled;
		bool https;
		bool savepw;
		bool initialized;
		bool sendfrom;
		Statusnet *sn;
};

		static EwAccountList* Instance();
		Statusnet* SnInstance(int accountid);

		std::list<EwAccount> getAccounts();
		EwAccount getAccount(int id);
		EwAccount getAccount(std::string identifier);
		//std::string getApi(EwAccount ewa);
		void setAccountEnabled(int id);
		void setAccountEnabled(std::string identifier);
		int getLastId();
		bool removeAccount(std::string identifier);
		bool removeAccount(int id);
		int addAccount(std::string identifier);
		int addAccount();
		std::string getAccountIdentifier(int id);
		void writeToFile();
		bool updateAccountIdentifier(int id, std::string identifier);
		bool updateAccountEnabled(int id, bool enabled);
		bool updateAccountHttps(int id, bool https);
		bool getAccountEnabled(int id);
		bool getAccountHttps(int id);
		int getIdToIdentifier(std::string identifier);
		int getIdOfFirstAccount();
		void setAccountPassword(int id, std::string password);
		std::string getAccountPassword(int id);
		void setAccountSendFrom(int id);

	private:
		Statusnet *defaultsn;
		EwAccountList();
		EwAccountList(EwAccountList const&) {};
		static EwAccountList* m_pInstance;
		bool initialized;

		std::list<EwAccount> accounts;
		void readFromFile();
		EwAccount parseXmlAccount(const xmlpp::Element* account);
		std::string inttostr(int i);
};

#endif
