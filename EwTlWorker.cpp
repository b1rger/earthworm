#include "EwTlWorker.h"
#include "EwLogging.h"
#include "EwTools.h"
#include "EwAccountList.h"
#include <functional>

EwTlWorker::EwTlWorker() {
	thread = 0;
	stop = false;
}

EwTlWorker::~EwTlWorker() {
	{
		Glib::Mutex::Lock lock (mutex);
		stop = true;
	}
	if (thread)
		thread->join();
}

void EwTlWorker::start(int accountid, EwTools::TimelineType type, std::list<Statusnet::Status>& timeline) {
	thread = Glib::Thread::create(sigc::bind(sigc::mem_fun(*this, &EwTlWorker::run), accountid, type, timeline), true);
}

void EwTlWorker::run(int accountid, EwTools::TimelineType type, std::list<Statusnet::Status>& timeline) {
	EWLOG("In EwTlWorker::run()", 8);
	while (true) {
		{
			Glib::Mutex::Lock lock (mutex);
			if (stop) break;
		}
		switch (this->type) {
			case EwTools::Tpublic:
				//return sn->getPublicTimeline();
				timeline = EwAccountList::Instance()->SnInstance(accountid)->getPublicTimeline();
			case EwTools::Thome:
				//return sn->getHomeTimeline();
			case EwTools::Tmentions:
				//return sn->getMentions();
			case EwTools::Tuser:
				//return sn->getUserTimeline();
			case EwTools::Tretweetofme:
				//return sn->getRetweetsOfMe();
			default:
				break;
		}
		std::cout<<"Sending loop signal"<<std::endl;
		sig_loop();
		sleep(10);
		std::cout << "Thread write!" << std::endl;
	}
}

