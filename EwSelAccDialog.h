#include <gtkmm.h>

class EwSelAccDialog {
	public:
		EwSelAccDialog();
		int getId();
	private:
		void on_okbtn_clicked();
		void on_cncbtn_clicked();
		void onrbtnclicked(int id);
		int id;
		Gtk::Dialog *dialog;

		std::string inttostr(int i);
};
