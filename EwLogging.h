#ifndef EW_LOGGING_H
#define EW_LOGGING_H
#include <string>
#define EWLOG EwLogging::Instance()->log

class EwLogging {
	public:
		static EwLogging* Instance();

		void log(std::string message, int lvl);
		void setLvl(int lvl);

	private:
		EwLogging();
		EwLogging(EwLogging const&){};
//		EwLogging& operator=(EwLogging&){};
		static EwLogging* m_pInstance;
		int lvl;
};
#endif
