#ifndef EW_AUTH_DIALOG_H
#define EW_AUTH_DIALOG_H
#include "EwPreferences.h"
#include "EwAccountList.h"
#include <gtkmm.h>

class EwAuthDialog {
	public:
		EwAuthDialog(int accountid);
		bool pwSet();

	private:
		Gtk::Dialog *dialog;
		Gtk::Button *okbutton, *cancelbutton;
		Gtk::Entry *passwordentry;
		int accountid;
		bool pwset;
		void okButtonClicked();
		void cancelButtonClicked();
};
#endif
