#include <EwSelAcc.h>
#include <EwLogging.h>
#include <EwAccountList.h>

EwSelAcc::EwSelAcc(Gtk::Window *parent) {
	Glib::RefPtr<Gtk::Builder> builder = Gtk::Builder::create_from_file("data/EwSelAcc.glade");
	Gtk::Box *rbtnbox;
	Gtk::RadioButton::Group rbgroup;
	Gtk::Button *okbtn, *cncbtn;
	builder->get_widget("ewselaccwin", ewselaccwin);
	builder->get_widget("rbtnbox", rbtnbox);
	builder->get_widget("okbtn", okbtn);
	builder->get_widget("cncbtn", cncbtn);
	id = -1;

	std::list<EwAccountList::EwAccount> accounts = EwAccountList::Instance()->getAccounts();
	for (std::list<EwAccountList::EwAccount>::iterator iter = accounts.begin(); iter != accounts.end(); iter++) {
		if (iter->enabled) {
			if (id==-1) 
				id = iter->id;
			Gtk::RadioButton *account = new Gtk::RadioButton(rbgroup, (*iter).identifier);
			account->signal_clicked().connect(sigc::bind(sigc::mem_fun(*this, &EwSelAcc::onrbtnclicked), (*iter).id));
			rbtnbox->pack_start(*account);
		}
	}

	okbtn->signal_clicked().connect(sigc::mem_fun(*this, &EwSelAcc::on_okbtn_clicked));
	cncbtn->signal_clicked().connect(sigc::mem_fun(*this, &EwSelAcc::on_cncbtn_clicked));

	ewselaccwin->set_transient_for(*parent);
	ewselaccwin->show_all_children();
	Gtk::Main::run(*ewselaccwin);
}

void EwSelAcc::on_okbtn_clicked() {
	ewselaccwin->hide();
}

void EwSelAcc::on_cncbtn_clicked() {
	this->id = -1;
	ewselaccwin->hide();
}

void EwSelAcc::onrbtnclicked(int id) {
	EWLOG("EwSelAcc::onrbtnclicked: "+inttostr(id), 8);
	this->id = id;
}

int EwSelAcc::getId() {
	EWLOG("EwSelAcc::getId()", 8);
	return this->id;
}

std::string EwSelAcc::inttostr(int i) {
	std::stringstream intstream;
	intstream<<i;
	return intstream.str();
}
