#include <gtkmm.h>
#include <EwAccountList.h>
#include <libxml++/libxml++.h>

class EwAccountsWin {
	public:
		EwAccountsWin();
	protected:
		class ModelColumns : public Gtk::TreeModel::ColumnRecord {
			public:
				ModelColumns() {
					add(m_col_enab);
					add(m_col_identifier);
					add(m_col_id);
					add(m_col_https);
				}
				Gtk::TreeModelColumn<bool> m_col_enab, m_col_https;
				Gtk::TreeModelColumn<std::string> m_col_identifier;
				Gtk::TreeModelColumn<unsigned int> m_col_id;
		};
		ModelColumns m_Columns;
		Gtk::TreeView treeView;
		Glib::RefPtr<Gtk::ListStore> liststore;

		void onAddBtnClicked();
		void onCloseBtnClicked();
		void onDelBtnClicked();
		void onColumnIdentifierChanged(std::string path, std::string newtext);
		void onColumnEnabledToggled(std::string path);
		void onColumnHttpsToggled(std::string path);
	private:
		//EwAccount ewa;
		//std::list<EwAccount> tmpaccounts;
		Gtk::Window *window;

		void writeToFile();
		void readFromFile();
		EwAccountList::EwAccount parseAccount(const xmlpp::Element* account);
		void renewAccount(int id, std::string username, std::string server, bool active);
		std::string inttostr(int i);
};
