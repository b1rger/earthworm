#include "StatusWidget.h"
#include <iostream>
#include <string>
#include <fstream>
#include <streambuf>
#include <webkit/webkit.h>
#include <glib.h>

StatusWidget::StatusWidget(Statusnet::Status status) {
	GtkWidget *webkit = webkit_web_view_new();
	//std::cout<<"creating statuswidget with text: "<<status.text<<std::endl;
	WebKitWebView *view = WEBKIT_WEB_VIEW(webkit);
	Gtk::Widget *webkit_mm = Glib::wrap(webkit);
	WebKitWebSettings *webkitsettings = webkit_web_settings_new();
	g_object_set(G_OBJECT(webkitsettings), "enable-default-context-menu", FALSE, NULL);
	webkit_web_view_set_settings(WEBKIT_WEB_VIEW(webkit), webkitsettings);
	webkit_web_view_load_string(WEBKIT_WEB_VIEW(webkit), this->parse(status).c_str(), NULL, NULL, "");
	g_signal_connect(view, "navigation-policy-decision-requested", G_CALLBACK(&StatusWidget::navigation_policy_decision_requested_cb), view);
	webkit_mm->set_size_request(10, 100);

	this->add(*webkit_mm);
}

std::string StatusWidget::parse(Statusnet::Status status) {
//	std::cout<<"parsing status"<<std::endl;
	Statusnet::User user = status.user;
	std::string templatestring = this->getStringFromFile("data/status.tpl");
	templatestring = this->templateReplace(templatestring, "{{screen_name}}", user.screen_name);
	templatestring = this->templateReplace(templatestring, "{{avatar}}", user.profile_img_url);
	templatestring = this->templateReplace(templatestring, "{{time}}", status.created_at);
	templatestring = this->templateReplace(templatestring, "{{content}}", status.statusnet_html);
	templatestring = this->templateReplace(templatestring, "{{by_who}}", user.name);
	templatestring = this->templateReplace(templatestring, "{{name}}", user.name);
	
	return templatestring;
}

std::string StatusWidget::getStringFromFile(std::string filename) {
	std::ifstream t(filename.c_str());
	std::stringstream buffer;
	buffer << t.rdbuf();
	return buffer.str();
}

std::string StatusWidget::templateReplace(std::string haystack, std::string needle, std::string newstring) {
	while (haystack.find(needle)!=std::string::npos) {
		haystack = haystack.replace(haystack.find(needle), needle.length(), newstring);
	}
	return haystack;
}

gboolean StatusWidget::navigation_policy_decision_requested_cb(WebKitWebView* web_view, WebKitWebFrame* web_frame, WebKitNetworkRequest* request, WebKitWebNavigationAction* action, WebKitWebPolicyDecision* decision) {
	g_return_val_if_fail (WEBKIT_IS_WEB_VIEW (web_view), FALSE);
	std::cout<<webkit_network_request_get_uri(request)<<std::endl;
	GError *error;
	error = NULL;
	gtk_show_uri (gdk_screen_get_default(),webkit_network_request_get_uri(request), gtk_get_current_event_time(), &error);

	webkit_web_policy_decision_ignore(decision);
	return TRUE;
}
