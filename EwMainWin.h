#include <gtkmm.h>
#include "EwTools.h"

class EwMainWin { //: public Gtk::Window {
	public:
		EwMainWin();
	protected:
		//signal handlers
		void onManageAccountsClicked();
		void onPtimelineClicked();
		void onSendFromBtnClicked();
		void createTimeLinePage(EwTools::TimelineType type);
		void tabClose(int pagenum);
		void sendFromToggled(int id);
		void sendStatus();
	private:
		Gtk::Window *window;
		Gtk::Label *polabel;
		Gtk::Notebook *notebook;
		Gtk::Menu *sendfrommenu;
		Gtk::Entry *statusentry;
//		std::list<EwAccount> accounts;
};
