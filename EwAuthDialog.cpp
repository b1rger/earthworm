#include "EwAuthDialog.h"

EwAuthDialog::EwAuthDialog(int accountid) {
	this->accountid = accountid;
	this->pwset = false;
	Glib::RefPtr<Gtk::Builder> builder = Gtk::Builder::create_from_file("data/EwAuthDialog.glade");
	builder->get_widget("dialog", dialog);
	builder->get_widget("passwordentry", passwordentry);
	builder->get_widget("okbutton", okbutton);
	builder->get_widget("cancelbutton", cancelbutton);

	okbutton->signal_clicked().connect(sigc::mem_fun(this, &EwAuthDialog::okButtonClicked));
	cancelbutton->signal_clicked().connect(sigc::mem_fun(this, &EwAuthDialog::cancelButtonClicked));

	int result = dialog->run();
}

void EwAuthDialog::okButtonClicked() {
	EwAccountList::Instance()->setAccountPassword(accountid, passwordentry->get_text());
	this->pwset = true;
	dialog->response(Gtk::RESPONSE_OK);
	dialog->hide();
}

void EwAuthDialog::cancelButtonClicked() {
	dialog->response(Gtk::RESPONSE_CANCEL);
	dialog->hide();
}

bool EwAuthDialog::pwSet() {
	return this->pwset;
}
